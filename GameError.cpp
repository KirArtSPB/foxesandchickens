#include "GameError.h"

GameError::GameError(const char *message)
	: std::logic_error(message)
{
}