#include "GameUI.h"

const int GameUI::CELL_SIZE = 75;

void GameUI::drawBitmap(HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
{
	// метод рисования изображения в графическом контексте
	BITMAP bm;
	HDC hdcMem;
	POINT ptSize, ptOrg;
	hdcMem = CreateCompatibleDC(hdc);
	SelectObject(hdcMem, hBitmap);
	SetMapMode(hdcMem, GetMapMode(hdc));
	GetObject(hBitmap, sizeof(BITMAP), (LPVOID) &bm);
	ptSize.x = bm.bmWidth;
	ptSize.y = bm.bmHeight;
	DPtoLP(hdc, &ptSize, 1);
	ptOrg.x = 0;
	ptOrg.y = 0;
	DPtoLP(hdcMem, &ptOrg, 1);
	BitBlt(
		hdc, xStart, yStart, ptSize.x, ptSize.y,
		hdcMem, ptOrg.x, ptOrg.y, SRCCOPY
	);
	DeleteDC(hdcMem);
}

GameUI::GameUI(HWND hWnd)
{
	this->hWnd = hWnd;
	left = 0;
	top = 0;
	gameContext = new GameContext();
	restart();
}

GameUI::~GameUI()
{
	delete gameContext;
}

GameUI * GameUI::create(HWND hWnd)
{
	return new GameUI(hWnd);
}

void GameUI::setFoxBitmap(HBITMAP hBitmap)
{
	foxHBitmap = hBitmap;
}

void GameUI::setChickenBitmap(HBITMAP hBitmap)
{
	chickenHBitmap = hBitmap;
}

void GameUI::draw(HDC hdc)
{
	// заливка фона тёмно-серым цветом
	HBRUSH hBrush = CreateSolidBrush(RGB(64, 64, 64));
	RECT rect;
	GetClientRect(hWnd, &rect);
	SelectObject(hdc, hBrush);
	FillRect(hdc, &rect, hBrush);
	DeleteObject(hBrush);

	// цвет клетки - белый
	HBRUSH cellBrush = CreateSolidBrush(RGB(255, 255, 255));
	// цвет выделения клетки - светло-серый
	HBRUSH selectedBrush = CreateSolidBrush(RGB(192, 192, 192));
	// цвет клетки с победителем - зелёный
	HBRUSH greenBrush = CreateSolidBrush(RGB(0, 255, 0));
	// цвет клетки с при ничье - жёлтый
	HBRUSH stalemateBrush = CreateSolidBrush(RGB(255, 255, 0));

	for (int row = 0; row < GameContext::ROWS_COUNT; row++)
	{
		for (int col = 0; col < GameContext::COLS_COUNT; col++)
		{			
			int cellLeft = left + col * CELL_SIZE;
			int cellTop = top + row * CELL_SIZE;
			int cellRight = left + col * CELL_SIZE + CELL_SIZE;
			int cellBottom = top + row * CELL_SIZE + CELL_SIZE;

			CellState cellState = gameContext->getCell(row, col);

			if (cellState != None) {
				// рисуется клетка
				SelectObject(hdc, cellBrush);
				Rectangle(hdc, cellLeft, cellTop, cellRight, cellBottom);

				// если клетка выделена, на ней рисуется квадрат выделения
				if (cellSelected && row == selectedRow && col == selectedCol) {
					SelectObject(hdc, selectedBrush);
					Rectangle(hdc, cellLeft + 2, cellTop + 2, cellRight - 2, cellBottom - 2);
				}
			}

			if (cellState == Chicken || cellState == Fox) {
				// если ничья, у всех рисуется квадрат
				if (gameContext->getGameState() == Stalemate) {
					SelectObject(hdc, stalemateBrush);
					Rectangle(hdc, cellLeft + 2, cellTop + 2, cellRight - 2, cellBottom - 2);
				}
			}

			if (cellState == Chicken) {
				// если куры выиграли, рисуется квадрат победителя
				if (gameContext->getGameState() == ChickensVictory) {
					SelectObject(hdc, greenBrush);
					Rectangle(hdc, cellLeft + 2, cellTop + 2, cellRight - 2, cellBottom - 2);
				}

				// рисуется кура
				drawBitmap(hdc, chickenHBitmap, cellLeft + 5, cellTop + 5);
			}

			if (cellState == Fox) {
				// если лисы выиграли, рисуется квадрат победителя
				if (gameContext->getGameState() == FoxesVictory) {
					SelectObject(hdc, greenBrush);
					Rectangle(hdc, cellLeft + 2, cellTop + 2, cellRight - 2, cellBottom - 2);
				}

				// рисуется лиса
				drawBitmap(hdc, foxHBitmap, cellLeft + 5, cellTop + 5);
			}
		}
	}

	DeleteObject(cellBrush);
	DeleteObject(selectedBrush);
	DeleteObject(greenBrush);
	DeleteObject(stalemateBrush);

	// если сторона не выбрана, показывается окно для выбора
	if (!autoFox && !autoChicken) {
		// цвет окна выбора - сине-серый
		HBRUSH chooseBrush = CreateSolidBrush(RGB(128, 160, 192));

		int centerX = left + GameContext::COLS_COUNT * CELL_SIZE / 2;
		int centerY = top + GameContext::ROWS_COUNT * CELL_SIZE / 2;

		SelectBrush(hdc, chooseBrush);
		Rectangle(hdc, centerX - 150, centerY - 75, centerX + 150, centerY + 75);

		drawBitmap(hdc, chickenHBitmap, centerX - 100 + 5, centerY - CELL_SIZE / 2 + 5);
		drawBitmap(hdc, foxHBitmap, centerX + 100 - CELL_SIZE + 5, centerY - CELL_SIZE / 2 + 5);

		DeleteObject(chooseBrush);
	}
}

void GameUI::click(int x, int y) {
	// если сторона не выбрана, делается выбор;
	// за противоположную играет компьютер
	if (!autoChicken && !autoFox) {
		int centerX = left + GameContext::COLS_COUNT * CELL_SIZE / 2;
		int centerY = top + GameContext::ROWS_COUNT * CELL_SIZE / 2;

		if (x >= centerX - 100 && x < centerX - 100 + CELL_SIZE &&
			y >= centerY - CELL_SIZE / 2 && y < centerY + CELL_SIZE / 2) {
			autoFox = true;
		}
		else if (x >= centerX + 100 - CELL_SIZE && x < centerX + 100 &&
			y >= centerY - CELL_SIZE / 2 && y < centerY + CELL_SIZE / 2) {
			autoChicken = true;
		}
		else return;
	}

	else {
		if (x >= left && x < left + GameContext::COLS_COUNT * CELL_SIZE &&
			y >= top && y < top + GameContext::ROWS_COUNT * CELL_SIZE) {

			int row = (y - top) / CELL_SIZE;
			int col = (x - left) / CELL_SIZE;
			if (gameContext->getCell(row, col) != None) {
				// если щелчок по лисам или курам во время их хода, клетка выделяется
				if (gameContext->getCell(row, col) == Fox && gameContext->getGameState() == FoxesTurn ||
					gameContext->getCell(row, col) == Chicken && gameContext->getGameState() == ChickensTurn) {
					selectedCol = col;
					selectedRow = row;
					cellSelected = true;
				}

				// если лиса или кура уже выбрана, в указанную клетку делается ход
				else if (cellSelected) {
					GameState gameState = gameContext->getGameState();
					try {
						if (gameState == FoxesTurn) {
							gameContext->moveFox(selectedRow, selectedCol, row, col);
						}
						else if (gameState == ChickensTurn) {
							gameContext->moveChicken(selectedRow, selectedCol, row, col);
						}

						if (gameState == gameContext->getGameState()) {
							selectedCol = col;
							selectedRow = row;
						}
						else {
							cellSelected = false;
						}

						int count = 0;
						while (gameContext->getGameState() == FoxesTurn && false) {
							count++; if (count > 100) return;
							gameContext->moveFoxAuto();
							cellSelected = false;
						}
					}
					catch (GameError error) {
						// при нарушении правил игры, появляется окно с сообщением
						size_t len = strlen(error.what()) + 1;
						WCHAR *lstr = new WCHAR[len];
						MultiByteToWideChar(0, 0, error.what(), len, lstr, len);
						MessageBox(hWnd, lstr, L"Error", MB_OK | MB_ICONEXCLAMATION);
					}
				}
			}
			else {
				cellSelected = false;
			}
		}
	}

	// автоматический ход
	// count - защита от зацикливания
	int count = 0;
	while (gameContext->getGameState() == ChickensTurn && autoChicken ||
		gameContext->getGameState() == FoxesTurn && autoFox) {
		count++; if (count > 100) break;
		if (gameContext->getGameState() == ChickensTurn)
			gameContext->moveChickenAuto();
		else if (gameContext->getGameState() == FoxesTurn)
			gameContext->moveFoxAuto();
		cellSelected = false;
	}
}

void GameUI::resize(int width, int height) {
	// при изменении размера окна, игровое поле перемещатся в центр
	left = (width - GameContext::COLS_COUNT * CELL_SIZE) / 2;
	top = (height - GameContext::ROWS_COUNT * CELL_SIZE) / 2;
}

void GameUI::restart() {
	gameContext->reset();
	cellSelected = false;
	autoFox = false;
	autoChicken = false;
}