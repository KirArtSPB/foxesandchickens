#pragma once
#include <vector>

// состояния клеток игрового поля
enum CellState {
	// исключена из игрового процесса
	None = 0,
	// пустая
	Empty, 
	// занята лисой
	Fox,
	// занята курой
	Chicken
};

// состояния игры
enum GameState {
	// ход кур
	ChickensTurn,
	// ход лис
	FoxesTurn,
	// куры выиграли
	ChickensVictory,
	// лисы выиграли
	FoxesVictory,
	// ничья
	Stalemate
};

struct Move {
	int fromRow;
	int fromCol;
	int toRow;
	int toCol;

	Move(int fromRow, int fromCol, int toRow, int toCol)
		: fromRow(fromRow), fromCol(fromCol), toRow(toRow), toCol(toCol) { }
};

// Игровой контекст
// Содержит прямоугольное игровое поле, состоящее из клеток,
// единственным свойством которых является состояние.
// часть клеток исключены из игрового процесса.
// Положения кур и лис определяются клетками с соответсвующими состояниями.
class GameContext
{
	// массив клеток игрового поля (их состояния)
	CellState *cells;
	// состояние игры
	GameState gameState;

	// длина самой длинной цепочки съедения
	int maxChainLength;
	// положение лисы, продолжающей цепочку съедения
	int foxCatchingRow;
	int foxCatchingCol;
	// длина текущей цепочки съедения
	int foxCatchingChainLength;
	// признак того, что лиса продолжает цепочку съедения
	bool foxIsCathing;

	// составляет список возможных ходов
	std::vector<Move> possibleMoves;

	// устанавливает состояние клетки игрового поля
	void setCell(int row, int col, CellState state);
	// находит самую длинную цепочку для данной лисы
	int findMaxChainLength(int row, int col);

	// завершает ход
	void endTurn();
	// инициирует ход лисы
	void prepareFoxesTurn();

	void findPossibleMoves();

	// выбрасывает ошибку, координаты клетки за пределами диапазона
	void validateCellPosition(int row, int col) const;
	// возвращает true, если куры выполнили условие игры
	bool chickensHidden() const;
	// возвращает true, если куры больше не способны выполнить условие игры
	bool chickensCaught() const;

public:
	// инициализирует игровой контекст
	GameContext();
	// освобождает занятые ресурсы
	~GameContext();

	// количество строк и столбцов в игровом поле
	static const int ROWS_COUNT;
	static const int COLS_COUNT;

	// сбрасывает состояние игры
	void reset();

	// возвращает состояние клетки игрового поля
	CellState getCell(int row, int col) const;
	// возвращает состояние игры
	GameState getGameState() const;

	// выполняет ход выбранной лисы в выбранную клетку
	void moveFox(int fromRow, int fromCol, int toRow, int toCol);
	// выполняет ход лисы автоматически
	void moveFoxAuto();
	// выполняет ход выбранной куры в выбранную клетку
	void moveChicken(int fromRow, int fromCol, int toRow, int toCol);
	// выполняет ход куры автоматически
	void moveChickenAuto();
};

