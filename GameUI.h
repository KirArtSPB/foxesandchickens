#pragma once
#include "GameContext.h"
#include "framework.h"
#include "GameError.h"

// Графический пользовательский интерфейс игры.
// Обеспечивает отображение игры на экране и взаимодействие пользователя с ней.
class GameUI
{
	// создаёт и инициализирует новый интерфейс игры
	GameUI(HWND hWnd);

	// дескриптор главного окна
	HWND hWnd;
	
	// положение игрового поля
	int left;
	int top;

	// игровой контекст
	GameContext *gameContext;

	// выбранная клетка
	bool cellSelected;
	int selectedRow;
	int selectedCol;

	// автоматические ходы
	bool autoFox;
	bool autoChicken;

	// дескрипторы изображений
	HBITMAP foxHBitmap;
	HBITMAP chickenHBitmap;

	// размер клеток
	const static int CELL_SIZE;

	// рисует изображение
	static void drawBitmap(HDC hdc, HBITMAP hBitmap, int xStart, int yStart);

public:

	// создаёт и инициализирует новый интерфейс игры и возвращает указатель на него
	static GameUI* create(HWND hWnd);
	// освобождает занятые ресурсы
	~GameUI();

	// устанавливает изображение лисы
	void setFoxBitmap(HBITMAP hBitmap);
	// устанавливает изображение куры
	void setChickenBitmap(HBITMAP hBitmap);
	// рисует игровое поле в заданном графическом контексте
	void draw(HDC hdc);
	// обрабатывает нажатие кнопки мыши
	void click(int x, int y);
	// адаптирует игровое поле по размеру экрана
	void resize(int width, int height);
	// перезапускает игру
	void restart();
};

