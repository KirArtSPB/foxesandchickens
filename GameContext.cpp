#include "GameContext.h"
#include "GameError.h"
#include <stdexcept>
#include <ctime>

const int GameContext::COLS_COUNT = 7;
const int GameContext::ROWS_COUNT = 7;

void GameContext::setCell(int row, int col, CellState state)
{
	cells[row * COLS_COUNT + col] = state;
}

void GameContext::validateCellPosition(int row, int col) const
{
	if (row < 0 || row >= ROWS_COUNT || col < 0 || col >= COLS_COUNT)
		throw GameError("The specified cell position is out of range.");
}

bool GameContext::chickensHidden() const
{
	// проверяется квадрат из 9 клеток в верхней части поля
	// если хоть одна из клеток не занята курой, куры не выполнили условие
	for (int row = 0; row <= 2; row++)
	{
		for (int col = 2; col <= 4; col++)
		{
			if (getCell(row, col) != Chicken)
				return false;
		}
	}
	return true;
}

bool GameContext::chickensCaught() const
{
	// подсчитывает количество кур
	int count = 0;
	for (int row = 0; row < ROWS_COUNT; row++)
	{
		for (int col = 0; col < COLS_COUNT; col++)
		{
			if (getCell(row, col) == Chicken)
				count++;
		}
	}
	return count < 9;
}

GameContext::GameContext()
{
	// все клетки поля хранятся в одномерном массиве
	// для доступа к клеткам по координатам используются методы getCell и setCell
	cells = new CellState[ROWS_COUNT * COLS_COUNT];
	reset();
	// инициализация генератора случайных чисел
	srand(time(0));
}

GameContext::~GameContext()
{
	delete[] cells;
}

void GameContext::reset()
{
	// начальная расстановка кур и лис в соответствии с правилами игры
	for (int row = 0; row < ROWS_COUNT; row++)
	{
		for (int col = 0; col < COLS_COUNT; col++)
		{
			if (row >= 2 && row <= 4 || col >= 2 && col <= 4) {
				if (row >= 3)
					setCell(row, col, Chicken);
				else if (row == 2 && (col == 2 || col == 4))
					setCell(row, col, Fox);
				else
					setCell(row, col, Empty);
			}
			else
				setCell(row, col, None);
		}
	}

	gameState = ChickensTurn;

	findPossibleMoves();
}

CellState GameContext::getCell(int row, int col) const
{
	validateCellPosition(row, col);

	return cells[row * COLS_COUNT + col];
}

GameState GameContext::getGameState() const
{
	return gameState;
}

int GameContext::findMaxChainLength(int row, int col)
{
	if (getCell(row, col) != Fox)
		return 0;

	// рекурсивный поиск длинных цепочек съедения

	int maxChainLength = 0;

	// четыре направления съедения
	for (int i = 0; i < 4; i++) {
		int chickenRow = row;
		int chickenCol = col;
		int destinationRow = row;
		int destinationCol = col;

		if (i == 0) {
			if (row - 2 < 0)
				continue;

			chickenRow -= 1;
			destinationRow -= 2;
		}
		else if (i == 1) {
			if (row + 2 >= ROWS_COUNT)
				continue;

			chickenRow += 1;
			destinationRow += 2;
		}
		else if (i == 2) {
			if (col - 2 < 0)
				continue;

			chickenCol -= 1;
			destinationCol -= 2;
		}
		else if (i == 3) {
			if (col + 2 >= COLS_COUNT)
				continue;

			chickenCol += 1;
			destinationCol += 2;
		}

		// если в соседней клетке кура, а за ней пусто,
		// выполняется съедение, определяется длина цепочки из новой позиции;
		// затем ход отменяется; +1 к самой длинной цепочке из возможных
		if (getCell(chickenRow, chickenCol) == Chicken && getCell(destinationRow, destinationCol) == Empty) {
			setCell(row, col, Empty);
			setCell(chickenRow, chickenCol, Empty);
			setCell(destinationRow, destinationCol, Fox);

			int chainLength = findMaxChainLength(destinationRow, destinationCol) + 1;
			if (chainLength > maxChainLength)
				maxChainLength = chainLength;

			setCell(row, col, Fox);
			setCell(chickenRow, chickenCol, Chicken);
			setCell(destinationRow, destinationCol, Empty);
		}
	}

	// если из данной позиции съесть куру невозможно, возвращается 0
	return maxChainLength;
}

void GameContext::prepareFoxesTurn()
{
	// метод вызывает только перед первым съедением в цепочке
	// рассчитывает длину самой большой цепочки
	// лиса должна будет пойти по любой цепочке такой же длины
	maxChainLength = 0;
	for (int row = 0; row < ROWS_COUNT; row++)
	{
		for (int col = 0; col < COLS_COUNT; col++)
		{
			if (getCell(row, col) == Fox) {
				int chainLength = findMaxChainLength(row, col);
				if (chainLength > maxChainLength)
					maxChainLength = chainLength;
			}
		}
	}

	foxIsCathing = false;
	foxCatchingChainLength = 0;
}

void GameContext::findPossibleMoves() {
	possibleMoves.clear();

	for (int row = 0; row < ROWS_COUNT; row++) {
		for (int col = 0; col < COLS_COUNT; col++) {
			if (gameState == FoxesTurn && getCell(row, col) == Fox) {
				// четыре направления для лисы
				// ходить можно в соседнюю клетку, есть можно через одну клетку, в которой кура
				for (int i = 0; i < 4; i++) {
					bool canStep = true;
					int stepRow = row;
					int stepCol = col;
					bool canCatch = true;
					int catchRow = row;
					int catchCol = col;

					if (i == 0) {
						if (row - 1 < 0) canStep = false;
						if (row - 2 < 0) canCatch = false;

						stepRow -= 1;
						catchRow -= 2;
					}
					else if (i == 1) {
						if (row + 1 >= ROWS_COUNT) canStep = false;
						if (row + 2 >= ROWS_COUNT) canCatch = false;

						stepRow += 1;
						catchRow += 2;
					}
					else if (i == 2) {
						if (col - 1 < 0) canStep = false;
						if (col - 2 < 0) canCatch = false;

						stepCol -= 1;
						catchCol -= 2;
					}
					else if (i == 3) {
						if (col + 1 >= COLS_COUNT) canStep = false;
						if (col + 2 >= COLS_COUNT) canCatch = false;

						stepCol += 1;
						catchCol += 2;
					}

					// обычные ходы
					if (canStep && getCell(stepRow, stepCol) == Empty)
						possibleMoves.push_back(Move(row, col, stepRow, stepCol));

					// ходы со съедением
					if (canCatch && getCell(stepRow, stepCol) == Chicken && getCell(catchRow, catchCol) == Empty)
						possibleMoves.push_back(Move(row, col, catchRow, catchCol));
				}
			}
			else if (gameState == ChickensTurn && getCell(row, col) == Chicken) {
				// три направления для куры
				// кура ходит на соседнюю клетку
				for (int i = 0; i < 3; i++) {
					bool canStep = true;
					int stepRow = row;
					int stepCol = col;

					if (i == 0) {
						if (row - 1 < 0) canStep = false;

						stepRow -= 1;
					}
					else if (i == 1) {
						if (col - 1 < 0) canStep = false;

						stepCol -= 1;
					}
					else if (i == 2) {
						if (col + 1 >= COLS_COUNT) canStep = false;

						stepCol += 1;
					}

					if (canStep && getCell(stepRow, stepCol) == Empty)
						possibleMoves.push_back(Move(row, col, stepRow, stepCol));
				}
			}
		}
	}
}

void GameContext::moveFox(int fromRow, int fromCol, int toRow, int toCol)
{
	// проверка указанных координат
	validateCellPosition(fromRow, fromCol);
	validateCellPosition(toRow, toCol);

	// проверка очерёдности хода
	if (gameState != FoxesTurn)
		throw GameError("It's not foxes' turn.");

	// проверка присутствия лисы в клетке отправления
	if (getCell(fromRow, fromCol) != Fox)
		throw GameError("Specified source cell doesn't contain a fox.");

	// проверка исключённых клеток
	if (getCell(toRow, toCol) == None)
		throw GameError("The specified cell is out of bounds.");

	// проверка несовпадения клетки отправления и клетки назначения
	if (fromRow == toRow && fromCol == toCol)
		throw GameError("Source cell and distination cell can't be the same.");

	if (foxIsCathing) {
		// проверка, что та же диса продолжает начатую цепочку съедения
		if (fromRow != foxCatchingRow || fromCol != foxCatchingCol)
			throw GameError("The other fox must continue catching.");
	}

	// проверка по списку возможных ходов
	bool moveIsPossible = false;
	for (std::vector<Move>::iterator iterator = possibleMoves.begin(); iterator != possibleMoves.end(); iterator++)
	{
		Move move = *iterator;
		if (move.fromRow == fromRow && move.fromCol == fromCol && move.toRow == toRow && move.toCol == toCol)
			moveIsPossible = true;
	}

	if (!moveIsPossible)
		throw GameError("The fox can't reach the specified cell.");

	// если определена длины цепочки съедения
	if (maxChainLength > 0) {
		// лиса должна есть

		// проверка, что лиса выполняет удлинённый ход для съедения
		if (toRow == fromRow && toCol != fromCol - 2 && toCol != fromCol + 2 ||
			toCol == fromCol && toRow != fromRow - 2 && toRow != fromRow + 2)
			throw GameError("A fox must catch a chicken.");

		// проверка длины потенциальной цепочки:
		// из последующих возможных ходов должна складываться
		// цепочка максимально возможной длины;
		// максимально возможная длина определяется заранее;

		int chickenRow = (fromRow + toRow) / 2;
		int chickenCol = (fromCol + toCol) / 2;

		setCell(fromRow, fromCol, Empty);
		setCell(chickenRow, chickenCol, Empty);
		setCell(toRow, toCol, Fox);

		int chainLength = findMaxChainLength(toRow, toCol);

		if (foxCatchingChainLength + 1 + chainLength < maxChainLength) {
			// если дальнейшая цепочка не может достигнуть максимально возможной длины,
			// ход отменяется
			setCell(fromRow, fromCol, Fox);
			setCell(chickenRow, chickenCol, Chicken);
			setCell(toRow, toCol, Empty);
			throw GameError("The foxes must catch chickens by the longest chain.");
		}

		// лиса начинает или продолжает цепочку съедения
		foxCatchingChainLength++;
		foxCatchingRow = toRow;
		foxCatchingCol = toCol;
		foxIsCathing = true;

		// если максимально возможная длина цепочки достигнута, ход лисы прекращается
		if (foxCatchingChainLength == maxChainLength)
			endTurn();
		else
			findPossibleMoves();
	}
	else {
		// лиса делает простой ход
		setCell(fromRow, fromCol, Empty);
		setCell(toRow, toCol, Fox);
		endTurn();
	}
}

void GameContext::moveFoxAuto()
{
	if (maxChainLength > 0) {
		// если лиса дожлжна есть,
		// выполняется поиск цепочки, соответствующей максимальной длине
		bool moveIsPossible = false;
		for (std::vector<Move>::iterator iterator = possibleMoves.begin(); iterator != possibleMoves.end(); iterator++)
		{
			Move move = *iterator;

			if (move.fromRow + 2 != move.toRow && move.fromRow - 2 != move.toRow &&
				move.fromCol + 2 != move.toCol && move.fromCol - 2 != move.toCol)
				continue;
			
			int chickenRow = (move.fromRow + move.toRow) / 2;
			int chickenCol = (move.fromCol + move.toCol) / 2;

			if (getCell(chickenRow, chickenCol) == Chicken && getCell(move.toRow, move.toCol) == Empty) {
				setCell(move.fromRow, move.fromCol, Empty);
				setCell(chickenRow, chickenCol, Empty);
				setCell(move.toRow, move.toCol, Fox);

				int chainLength = findMaxChainLength(move.toRow, move.toCol) + 1 + foxCatchingChainLength;

				setCell(move.fromRow, move.fromCol, Fox);
				setCell(chickenRow, chickenCol, Chicken);
				setCell(move.toRow, move.toCol, Empty);

				if (chainLength == maxChainLength) {
					moveFox(move.fromRow, move.fromCol, move.toRow, move.toCol);
					return;
				}
			}
		}
	}
	else {
		// выбирается случайный возможный ход
		int moveIndex = rand() % possibleMoves.size();
		Move move = possibleMoves[moveIndex];
		moveFox(move.fromRow, move.fromCol, move.toRow, move.toCol);
	}
}

void GameContext::moveChicken(int fromRow, int fromCol, int toRow, int toCol)
{
	// проверка координат клеток
	validateCellPosition(fromRow, fromCol);
	validateCellPosition(toRow, toCol);

	// проверка очерёдности хода
	if (gameState != ChickensTurn)
		throw GameError("It's not chickens' turn.");

	// проверка присутствия куры в клетке отправления
	if (getCell(fromRow, fromCol) != Chicken)
		throw GameError("Specified source cell doesn't contain a chicken.");

	// проверка исключённых клеток
	if (getCell(toRow, toCol) == None)
		throw GameError("The specified cell is out of bounds.");

	// проверка несовпадения клетки отправления и клетки назначения
	if (fromRow == toRow && fromCol == toCol)
		throw GameError("Source cell and distination cell can't be the same.");

	// проверка по списку возможных ходов
	bool moveIsPossible = false;
	for (std::vector<Move>::iterator iterator = possibleMoves.begin(); iterator != possibleMoves.end(); iterator++)
	{
		Move move = *iterator;
		if (move.fromRow == fromRow && move.fromCol == fromCol && move.toRow == toRow && move.toCol == toCol)
			moveIsPossible = true;
	}

	if (!moveIsPossible)
		throw GameError("The chicken can't reach the specified cell.");

	// кура перемещается
	setCell(fromRow, fromCol, Empty);
	setCell(toRow, toCol, Chicken);

	endTurn();
}

void GameContext::moveChickenAuto()
{
	// выбирается случайный возможный ход
	int moveIndex = rand() % possibleMoves.size();
	Move move = possibleMoves[moveIndex];
	moveChicken(move.fromRow, move.fromCol, move.toRow, move.toCol);
}

void GameContext::endTurn() {
	static bool recursive = false;

	if (gameState == ChickensTurn) {
		if (chickensHidden()) {
			// куры выполнили условие - они выиграли
			gameState = ChickensVictory;
			return;
		}
		else {
			// ход подготавливается и передаётся лисам
			prepareFoxesTurn();
			gameState = FoxesTurn;
		}
	}
	else if (gameState == FoxesTurn) {
		if (chickensCaught()) {
			// куры съедены - лисы выиграли
			gameState = FoxesVictory;
				return;
		}
		else {
			// ход передаётся курам
			gameState = ChickensTurn;
		}
	}

	findPossibleMoves();
	// если нет возможных ходов
	if (possibleMoves.size() == 0) {
		if (recursive) {
			// нет ходов у обеих сторон
			gameState = Stalemate;
		}
		else {
			// ход сразу прекращается
			recursive = true;
			endTurn();
		}
	}

	recursive = false;
}