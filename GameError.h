#pragma once
#include <stdexcept>

// Нарушение правил игры
class GameError : public std::logic_error
{
public:
	GameError(const char *message);
};

